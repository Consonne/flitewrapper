/*************************************************************************/
/*                                                                       */
/*                  Language Technologies Institute                      */
/*                     Carnegie Mellon University                        */
/*                         Copyright (c) 2012                            */
/*                        All Rights Reserved.                           */
/*                                                                       */
/*  Permission is hereby granted, free of charge, to use and distribute  */
/*  this software and its documentation without restriction, including   */
/*  without limitation the rights to use, copy, modify, merge, publish,  */
/*  distribute, sublicense, and/or sell copies of this work, and to      */
/*  permit persons to whom this work is furnished to do so, subject to   */
/*  the following conditions:                                            */
/*   1. The code must retain the above copyright notice, this list of    */
/*      conditions and the following disclaimer.                         */
/*   2. Any modifications must be clearly marked as such.                */
/*   3. Original authors' names are not deleted.                         */
/*   4. The authors' names are not used to endorse or promote products   */
/*      derived from this software without specific prior written        */
/*      permission.                                                      */
/*                                                                       */
/*  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         */
/*  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      */
/*  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   */
/*  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      */
/*  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    */
/*  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   */
/*  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          */
/*  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       */
/*  THIS SOFTWARE.                                                       */
/*                                                                       */
/*************************************************************************/
/*             Author:  Alok Parlikar (aup@cs.cmu.edu)                   */
/*               Date:  July 2012                                        */
/*************************************************************************/
package net.shallowmallow.speech.tts.flite;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import java.util.Map;
import java.util.HashMap;

public class TTSDemo extends ListActivity implements OnClickListener, OnKeyListener, OnInitListener {
	private final static String LOG_TAG = "Flite_Java_" + TTSDemo.class.getSimpleName();

	private EditText mUserText;
	private ImageButton mSendButton;
	private Button mSaveButton;
	private ArrayAdapter<String> mAdapter;
	private ArrayAdapter<String> mVoiceAdapter;
    private ArrayAdapter<String> mRateAdapter;
	private ArrayAdapter<String> mLangAdapter;
	private ArrayList<Voice> mVoices;
	private ArrayList<String> mStrings = new ArrayList<String>();
    private ArrayList<String> mRates = new ArrayList<String>();
	private ArrayList<String> mLanguages = new ArrayList<String>();
	private Map <String, ArrayList<String>> mLangToVoices = new HashMap<String,ArrayList<String>>();
	private Spinner mVoiceSpinner;
	private Spinner mLangSpinner;
    private Spinner mRateSpinner;
    private CheckBox mCheckBoxLang;
	private CheckBox mCheckBoxCou;
    private SeekBar mSeekBarSpeed;
	private TextToSpeech mTts;
	private String mSelectedVoice = "";
	private Context mContext;


	@TargetApi(14)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;

		ArrayList<Voice> allVoices = CheckVoiceData.getVoices();
		mVoices = new ArrayList<Voice>();
		for(Voice vox:allVoices) {
			if (vox.isAvailable()) {
				mVoices.add(vox);
			}
		}

		mLanguages = new ArrayList<String>();

		for (Voice vox: mVoices) {
			if (!mLanguages.contains(vox.getLanguage()))
			{
				mLanguages.add(vox.getLanguage());
				mLangToVoices.put(vox.getLanguage(),new ArrayList<String>()  );
				mLangToVoices.get(vox.getLanguage()).add(vox.getDisplayName());
			}
			else{
				mLangToVoices.get(vox.getLanguage()).add(vox.getDisplayName());
			}

		}






		if (mVoices.isEmpty()) {
			// We can't demo anything if there are no voices installed.
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getString(R.string.error_no_voices));
			builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
					finish();
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
		else {
			// Initialize the TTS
			if (android.os.Build.VERSION.SDK_INT >=
					android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
				mTts = new TextToSpeech(this, this, "net.shallowmallow.speech.tts.flite");
			}
			else {
				mTts = new TextToSpeech(this, this);
			}
			mSelectedVoice = "";

		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mTts != null)
			mTts.shutdown();
	}

	private void buildUI() {

		final ArrayList<String> voiceNames = new ArrayList<String>();

		for (Voice vox: mVoices) {
		    voiceNames.add(vox.getDisplayName()); // vox.getVariant());

		}

		mVoiceAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item,
				voiceNames);



		mLangAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item,
				mLanguages);

		setContentView(R.layout.activity_tts_demo);
		mStrings.add("Click an item here to synthesize, or enter your own text below!");
		mStrings.add("A whole joy was reaping, but they've gone south, go fetch azure mike!");
		mStrings.add("हिन्दी संवैधानिक रूप से भारत की प्रथम राजभाषा और भारत की सबसे अधिक बोली और समझी जाने वाली भाषा है।");
		mStrings.add("महाराष्ट्र आणि गोवा ह्या राज्यांची मराठी ही अधिकृत राजभाषा आहे.");
		mStrings.add("Hello World, नमस्कार, வணக்கம், నమస్కారం");
		mStrings.add("La voix française est une voix de l'association Consonne.");

		mAdapter = new InputHistoryAdapter(this, R.layout.list_tts_history, mStrings);

		setListAdapter(mAdapter);


		mRates.add(getString(R.string.speed0));
		mRates.add(getString(R.string.speed1));
		mRates.add(getString(R.string.speed2));
		mRates.add(getString(R.string.speed3));
		mRates.add(getString(R.string.speed4));
		mRates.add(getString(R.string.speed5));

		mRateAdapter = new ArrayAdapter<String>(this,
							android.R.layout.simple_spinner_dropdown_item,
							mRates);


		mUserText = (EditText) findViewById(R.id.userText);
		mSendButton = (ImageButton) findViewById(R.id.sendButton);

		mVoiceSpinner = (Spinner) findViewById(R.id.voice);
		mVoiceSpinner.setAdapter(mVoiceAdapter);

		mLangSpinner = (Spinner) findViewById(R.id.language);
		mLangSpinner.setAdapter(mLangAdapter);

		mRateSpinner = (Spinner) findViewById(R.id.speechrate);
		mRateSpinner.setAdapter(mRateAdapter);
		mRateSpinner.setSelection(2);

		mSeekBarSpeed = (SeekBar) findViewById(R.id.seekBarSpeed);
		mSeekBarSpeed.setMax(200);

		mSaveButton = (Button) findViewById(R.id.saveButton);

		mSaveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				SharedPreferences.Editor editor = mContext.getSharedPreferences("test",0).edit();
				Voice vox = getSelectedVoice();
				editor.putInt(vox.getName()+"_speed", mSeekBarSpeed.getProgress());
				editor.apply();



			}
		});

		mRateSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int position, long arg3) {
				float rate =  (float) (position+ 1)/3 * 100;
				mSeekBarSpeed.setProgress((int) rate);


				mSeekBarSpeed.refreshDrawableState();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

		});

		mSeekBarSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
					//mRateSpinner.setSelection(5);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				mRateSpinner.setSelection(5);
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {


			}
		});


		mLangSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int position, long arg3) {
				System.out.println( mLangToVoices);
				ArrayList<String> voiceNames2 = mLangToVoices.get(String.valueOf(mLangSpinner.getSelectedItem()));

				ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,
						android.R.layout.simple_spinner_item, voiceNames2);
				dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				dataAdapter.notifyDataSetChanged();
				mVoiceSpinner.setAdapter(dataAdapter);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

		});



		mCheckBoxCou = (CheckBox) findViewById((R.id.checkBox2));
		mCheckBoxCou.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				SharedPreferences.Editor editor = mContext.getSharedPreferences("test",0).edit();
				String lang = String.valueOf(mLangSpinner.getSelectedItem());
				Voice vox = getSelectedVoice();

				String country = lang+"-"+ vox.getCountry();

				if (mCheckBoxCou.isChecked()){
					editor.putString(country, vox.getName());
					editor.apply();
				}
			}
		});

		mCheckBoxLang = (CheckBox) findViewById((R.id.checkBox));
		mCheckBoxLang.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				SharedPreferences.Editor editor = mContext.getSharedPreferences("test",0).edit();

				String lang = String.valueOf(mLangSpinner.getSelectedItem());
				Voice vox = getSelectedVoice();


				if (mCheckBoxLang.isChecked()){
					editor.putString(lang, vox.getName());
					editor.apply();
				}
				else{
					editor.putString(lang, "");
					editor.apply();
				}
			}
		});




		mUserText.setOnClickListener(this);
		mSendButton.setOnClickListener(this);
		mUserText.setOnKeyListener(this);

		mLangSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int position, long arg3) {
				System.out.println( mLangToVoices);
				ArrayList<String> voiceNames2 = mLangToVoices.get(String.valueOf(mLangSpinner.getSelectedItem()));

				String listString = "";

				ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,
						android.R.layout.simple_spinner_item, voiceNames2);
				dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				dataAdapter.notifyDataSetChanged();
				mVoiceSpinner.setAdapter(dataAdapter);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

		});

		mVoiceSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int position, long arg3) {
				Log.v("flite", "voice spinner");
				// Check if there is a preferred language
				SharedPreferences settings = mContext.getSharedPreferences("test",0);

				Voice vox = getSelectedVoice();
				String lang = String.valueOf(mLangSpinner.getSelectedItem());
				String voice_name = settings.getString(lang, "");

				if (vox == null) return;

				// if there is no preference suppose the first voice  is the default one
				Log.v("flite_preflang", voice_name);
				Log.v("flite_prefsellang", vox.getName());
				if (voice_name.equals(vox.getName())){ //String.valueOf(mVoiceSpinner.getSelectedItem()))){
					mCheckBoxLang.setChecked(true);
				}
				else{
					mCheckBoxLang.setChecked(false);
				}

				voice_name = settings.getString((lang+"-"+ vox.getCountry()), "");
				Log.v("flite_prefcountry", voice_name);

				if (voice_name.equals(vox.getName())){
					mCheckBoxCou.setChecked(true);
				}
				else{
					mCheckBoxCou.setChecked(false);
				}

				int speed = settings.getInt(vox.getName()+ "_speed", -1 );
				if (speed > 0){
					mSeekBarSpeed.setProgress(speed);
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

		});


	}

	public void onClick(View v) {
		sendText();
	}

	private void sendText() {
		String text = mUserText.getText().toString();
		if (text.isEmpty())
			return;
		mAdapter.add(text);
		mUserText.setText(null);
		sayText(text);
	}

	private void sayText(String text) {
		Log.v(LOG_TAG, "Speaking: " + text);
		int currentVoiceID = mVoiceSpinner.getSelectedItemPosition();
		String lang = String.valueOf(mLangSpinner.getSelectedItem());
		String voiceName = mLangToVoices.get(lang).get(currentVoiceID);


		if (! voiceName.equals(mSelectedVoice)) {
			mSelectedVoice = voiceName;
			for (Voice vox: mVoices) {
				if (vox.getDisplayName().equals(mSelectedVoice)){
					mTts.setLanguage(vox.getLocale());
				}
			}
		}
		Log.v("flite","Speaking: " + (float) mSeekBarSpeed.getProgress()/100);
		mTts.setSpeechRate((float) mSeekBarSpeed.getProgress()/100 );

		mTts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
	}

	private Voice getSelectedVoice(){
		int currentVoiceID = mVoiceSpinner.getSelectedItemPosition();
		String lang = String.valueOf(mLangSpinner.getSelectedItem());
		String voiceName = mLangToVoices.get(lang).get(currentVoiceID);
		for (Voice vox: mVoices) {
			if (vox.getDisplayName().equals(voiceName)){
				return vox;
			}
		}
		return null;

	}

	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			switch (keyCode) {
			case KeyEvent.KEYCODE_DPAD_CENTER:
			case KeyEvent.KEYCODE_ENTER:
				sendText();
				return true;
			}
		}
		return false;
	}

	private class InputHistoryAdapter extends ArrayAdapter<String> {
		private ArrayList<String> items;

		public InputHistoryAdapter(Context context,
				int textViewResourceId, ArrayList<String> items) {
			super(context, textViewResourceId, items);
			this.items = items;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = vi.inflate(R.layout.list_tts_history, null);
			}
			String s = items.get(position);
			TextView tt = (TextView) convertView.findViewById(R.id.inputText);
			tt.setText(s);
			return convertView;
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	public void onInit(int status) {
		boolean success = true;
		if (status == TextToSpeech.ERROR) {
			success = false;
		}

		if (success &&
				(android.os.Build.VERSION.SDK_INT >=
				android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)) {
			status = mTts.setEngineByPackageName("net.shallowmallow.speech.tts.flite");
		}

		if (status == TextToSpeech.ERROR) {
			success = false;
		}

		// REALLY check that it is flite engine that has been initialized
		// This is done using a hack, for now, since for API < 14
		// there seems to be no way to check which engine is being used.

		if (mTts.isLanguageAvailable(new Locale("eng", "USA", "is_flite_available"))
				!= TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE) {
			success = false;
		}

		if (!success) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Flite TTS Engine could not be initialized. Check that Flite is enabled on your phone!. In some cases, you may have to select flite as the default engine.");
			builder.setNegativeButton("Open TTS Settings", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
					Intent intent = new Intent();
					intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.TextToSpeechSettings"));
			        startActivity(intent);
					finish();
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
		else {
			buildUI();
		}
	}

	@Override
	public void onListItemClick(ListView parent, View view, int position, long id) {
		String text = (String) parent.getItemAtPosition(position);
		sayText(text);

	}
}
