/*************************************************************************/
/*                                                                       */
/*                  Language Technologies Institute                      */
/*                     Carnegie Mellon University                        */
/*                         Copyright (c) 2010                            */
/*                        All Rights Reserved.                           */
/*                                                                       */
/*  Permission is hereby granted, free of charge, to use and distribute  */
/*  this software and its documentation without restriction, including   */
/*  without limitation the rights to use, copy, modify, merge, publish,  */
/*  distribute, sublicense, and/or sell copies of this work, and to      */
/*  permit persons to whom this work is furnished to do so, subject to   */
/*  the following conditions:                                            */
/*   1. The code must retain the above copyright notice, this list of    */
/*      conditions and the following disclaimer.                         */
/*   2. Any modifications must be clearly marked as such.                */
/*   3. Original authors' names are not deleted.                         */
/*   4. The authors' names are not used to endorse or promote products   */
/*      derived from this software without specific prior written        */
/*      permission.                                                      */
/*                                                                       */
/*  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         */
/*  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      */
/*  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   */
/*  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      */
/*  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    */
/*  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   */
/*  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          */
/*  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       */
/*  THIS SOFTWARE.                                                       */
/*                                                                       */
/*************************************************************************/
/*             Author:  Alok Parlikar (aup@cs.cmu.edu)                   */
/*               Date:  April 2010                                       */
/*************************************************************************/

package net.shallowmallow.speech.tts.flite;

import java.io.File;
import java.util.ArrayList;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import android.widget.ImageButton;
import android.widget.Toast;
import 	android.widget.CheckedTextView;

/* Download user-requested voice data for Flite
 * 
 */
public class DownloadVoiceData extends ExpandableListActivity {
	private final static String LOG_TAG = "Flite_Java_" + DownloadVoiceData.class.getSimpleName(); //NON-NLS
	private MyExpandableAdapter mListAdapter;
	private Context mContext;
	// Create ArrayList to hold parent Items and Child Items

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		 // Create Expandable List and set it's properties
        ExpandableListView expandableList = getExpandableListView();
        expandableList.setDividerHeight(2);
        expandableList.setGroupIndicator(null);
        expandableList.setClickable(true);

		registerReceiver(onComplete,
				new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

		mListAdapter = new MyExpandableAdapter(this);
		this.setListAdapter(mListAdapter);
		mContext = this;
	}


	@Override
	public void onDestroy() {
		super.onDestroy();

		unregisterReceiver(onComplete);
	}

	public void onResume() {
		super.onResume();
		mListAdapter.refresh();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(R.string.voice_list_update);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Only works for a single menu option.
		// User must have requested a refresh of the voice list.

		Toast toast = Toast.makeText(mContext, getString(R.string.file_list_download2), Toast.LENGTH_SHORT);
		toast.show();

		Thread thread = new Thread() {
			@Override
			public void run() {
				CheckVoiceData.DownloadVoiceList(mContext, new Runnable() {
					@Override
					public void run() {
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								mListAdapter.refresh();

							}
						});
					}
				});

			}
		};

		thread.start();
		return true;

	}


	private class MyExpandableAdapter extends BaseExpandableListAdapter
	{
		private Context mContext;
		private ArrayList<Voice> mVoiceList;
		private LayoutInflater mInflater;
		private ArrayList<String> languages;  // parent items
		private ArrayList<ArrayList<Voice>> voicesByLang;

		public MyExpandableAdapter(Context context) {
			mContext = context;
			mInflater = LayoutInflater.from(mContext);

			// Get Information about voices
			mVoiceList = CheckVoiceData.getVoices();


			

			if (mVoiceList.isEmpty()) {
				Intent intent = new Intent(mContext, CheckVoiceData.class);
		        startActivity(intent);
			}

			organiseLists();
		}

		public void organiseLists(){

			languages = new ArrayList<String>();

			voicesByLang = new ArrayList<ArrayList<Voice>>();
			// Creating languages lists
			for (int i = 0; i < mVoiceList.size(); i++) {
				if (!languages.contains(mVoiceList.get(i).getLanguage()))
				{
					languages.add(mVoiceList.get(i).getLanguage());
				}

			}
			java.util.Collections.sort(languages, String.CASE_INSENSITIVE_ORDER);

			// adding each voice to each language
			for (int i = 0; i < languages.size(); i++) {
				ArrayList<Voice> voicesForLanguage = new ArrayList<Voice>();
				for (int j = 0; j < mVoiceList.size(); j++) {
					if (mVoiceList.get(j).getLanguage().equals(languages.get(i))){
						voicesForLanguage.add(mVoiceList.get(j));
					}
				}
				voicesByLang.add(voicesForLanguage);
			}
		}

   
	    // method getChildView is called automatically for each child view.
	    //  Implement this method as per your requirement
        @Override
		public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {



			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.view_voice_manager, parent, false);
			}

			((TextView) convertView.findViewById(R.id.voice_manager_voice_language)).setText(voicesByLang.get(groupPosition).get(childPosition).getDisplayLanguage());
			((TextView) convertView.findViewById(R.id.voice_manager_voice_variant)).setText(voicesByLang.get(groupPosition).get(childPosition).getVariant());

			String size = android.text.format.Formatter.formatShortFileSize(mContext, voicesByLang.get(groupPosition).get(childPosition).getByteSize());
			((TextView) convertView.findViewById(R.id.voice_size)).setText(size);



			//android.text.format.Formatter.formatShortFileSize(activityContext, bytes);




			final ImageButton actionButton = (ImageButton) convertView.findViewById(R.id.voice_manager_action_image);
			actionButton.setImageResource(
					voicesByLang.get(groupPosition).get(childPosition).isAvailable()?R.drawable.ic_action_delete:R.drawable.ic_action_download);
			actionButton.setVisibility(View.VISIBLE);

			actionButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					final Voice vox = voicesByLang.get(groupPosition).get(childPosition);
					if (!vox.isAvailable()) {
						AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
						builder.setMessage(getString(R.string.data_alert));
						builder.setCancelable(false);
						builder.setPositiveButton(getString(R.string.download_voice), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// Create destination directory
								File f = new File
										(vox.getPath());
								f.mkdirs();
								f.delete();
								String url = Voice.getDownloadURLBasePath() + vox.getName() + "." + vox.getFormat();
								DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
								request.setDescription(getString(R.string.downloading_voice) + vox.getName());
								request.setTitle(getString(R.string.download_voice_title));
								request.setDestinationUri(Uri.fromFile(new File(vox.getPath())));

								DownloadManager manager = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
								manager.enqueue(request);
								Toast toast = Toast.makeText(mContext, getString(R.string.download_started), Toast.LENGTH_SHORT);
								toast.show();
								actionButton.setVisibility(View.INVISIBLE);

							}
						});
						builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
						AlertDialog alert = builder.create();
						alert.show();
					}
					else {
						AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
						builder.setMessage(getString(R.string.sure_deleting) + vox.getDisplayName());
						builder.setCancelable(false);
						builder.setPositiveButton(getString(R.string.delete_voice), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								File f = new File(vox.getPath());
								if(f.delete()) {
									refresh();
									Toast toast = Toast.makeText(mContext, getString(R.string.voice_deleted), Toast.LENGTH_SHORT);
									toast.show();
								}
							}
						});
						builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
						AlertDialog alert = builder.create();
						alert.show();
					}
				}
			});

			convertView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					actionButton.performClick();
				}
			});

			return convertView;
		}


		public void refresh() {

			mVoiceList = CheckVoiceData.getVoices();
			organiseLists();
			notifyDataSetChanged();
		}

		// method getGroupView is called automatically for each parent item
		// Implement this method as per your requirement
		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
		{

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.downloadlist_language, null);
			}

			((CheckedTextView) convertView).setText(languages.get(groupPosition));
			((CheckedTextView) convertView).setChecked(isExpanded);

			return convertView;
		}

	    @Override
	    public Object getChild(int groupPosition, int childPosition)
	    {
	        return null;
	    }

	    @Override
	    public long getChildId(int groupPosition, int childPosition)
	    {
	        return 0;
	    }

	    @Override
	    public int getChildrenCount(int groupPosition)
	    {
	        return ((ArrayList<Voice>) voicesByLang.get(groupPosition)).size();
	    }

	    @Override
	    public Object getGroup(int groupPosition)
	    {
	        return null;
	    }

	    @Override
	    public int getGroupCount()
	    {
	        return languages.size();
	    }

	    @Override
	    public void onGroupCollapsed(int groupPosition)
	    {
	        super.onGroupCollapsed(groupPosition);
	    }

	    @Override
	    public void onGroupExpanded(int groupPosition)
	    {
	        super.onGroupExpanded(groupPosition);
	    }

	    @Override
	    public long getGroupId(int groupPosition)
	    {
	        return 0;
	    }

	    @Override
	    public boolean hasStableIds()
	    {
	        return false;
	    }

	    @Override
	    public boolean isChildSelectable(int groupPosition, int childPosition)
	    {
	        return false;
	    }
	}
	BroadcastReceiver onComplete=new BroadcastReceiver() {
		public void onReceive(Context ctxt, Intent intent) {
			Toast toast = Toast.makeText(ctxt, getString(R.string.voice_downloaded), Toast.LENGTH_SHORT);
			toast.show();
			mListAdapter.refresh();
		}
	};

}
