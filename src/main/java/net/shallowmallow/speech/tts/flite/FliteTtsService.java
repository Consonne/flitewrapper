/*************************************************************************/
/*                                                                       */
/*                  Language Technologies Institute                      */
/*                     Carnegie Mellon University                        */
/*                         Copyright (c) 2010                            */
/*                        All Rights Reserved.                           */
/*                                                                       */
/*  Permission is hereby granted, free of charge, to use and distribute  */
/*  this software and its documentation without restriction, including   */
/*  without limitation the rights to use, copy, modify, merge, publish,  */
/*  distribute, sublicense, and/or sell copies of this work, and to      */
/*  permit persons to whom this work is furnished to do so, subject to   */
/*  the following conditions:                                            */
/*   1. The code must retain the above copyright notice, this list of    */
/*      conditions and the following disclaimer.                         */
/*   2. Any modifications must be clearly marked as such.                */
/*   3. Original authors' names are not deleted.                         */
/*   4. The authors' names are not used to endorse or promote products   */
/*      derived from this software without specific prior written        */
/*      permission.                                                      */
/*                                                                       */
/*  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         */
/*  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      */
/*  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   */
/*  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      */
/*  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    */
/*  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   */
/*  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          */
/*  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       */
/*  THIS SOFTWARE.                                                       */
/*                                                                       */
/*************************************************************************/
/*             Author:  Alok Parlikar (aup@cs.cmu.edu)                   */
/*               Date:  June 2012                                        */
/*************************************************************************/

package net.shallowmallow.speech.tts.flite;

import net.shallowmallow.speech.tts.flite.NativeFliteTTS.SynthReadyCallback;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioFormat;
import android.speech.tts.SynthesisCallback;
import android.speech.tts.SynthesisRequest;
import android.speech.tts.TextToSpeechService;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import java.util.Locale;
import android.content.SharedPreferences;

import java.util.Collections;
import java.util.Set;

/**
 * Implements the Flite Engine as a TextToSpeechService
 *
 */

@TargetApi(21)
public class FliteTtsService extends TextToSpeechService {
	private final static String LOG_TAG = "Flite_Java_" + FliteTtsService.class.getSimpleName(); //NON-NLS
	private NativeFliteTTS mEngine;

	private static final String DEFAULT_LANGUAGE = "eng"; //NON-NLS
	private static final String DEFAULT_COUNTRY = "USA"; //NON-NLS
	private static final String DEFAULT_VARIANT = "male,rms"; //NON-NLS

	private String mCountry = DEFAULT_COUNTRY;
	private String mLanguage = DEFAULT_LANGUAGE;
	private String mVariant = DEFAULT_VARIANT;
	private Integer mSpeechRate = 100;
	private int defaultSpeechRate = 100;
	private SynthesisCallback mCallback;
	private Context mContext;


	@Override
	public void onCreate() {
		initializeFliteEngine();
		mContext = this;


		// This calls onIsLanguageAvailable() and must run after Initialization
		super.onCreate();
	}

	private void initializeFliteEngine() {
		if (mEngine != null) {
			mEngine.stop();
			mEngine = null;
		}
		mEngine = new NativeFliteTTS(this, mSynthCallback);
	}

//api 21
	@Override
	public String onGetDefaultVoiceNameFor (String lang, 
                String country, 
                String variant){
		Log.v(LOG_TAG, "onDefaultvoicename");

		ArrayList<Voice>  flitevoices = CheckVoiceData.getAvailableVoices() ;
		Log.v(LOG_TAG, "looking for" + lang + " " + country +" " + variant); //NON-NLS
		String voice_name = "";

		// Check if in Preferences

		SharedPreferences settings = mContext.getSharedPreferences("test",0); //NON-NLS

		if ((country == null) || (country == "")){
			voice_name = settings.getString(lang, "");
			if (voice_name != ""){
				Log.v("flite", "preferences are set to " + voice_name +  " for language" + lang ); //NON-NLS
			}
		}
		else{
			voice_name = settings.getString(lang+"-"+country, "");
			if (voice_name != ""){
				Log.v("flite", "preferences are set to " + voice_name +  " for language and country" + lang + " " + country ); //NON-NLS
			}
		}

		if (voice_name != ""){
			for (Voice vox:flitevoices) {
				if (vox.getName().equals(voice_name)){
					return voice_name;
				}
			}
			Log.v("flite", "voice name set in preferences isn't available" ); //NON-NLS
		}
		else{
			Log.v("flite", "no voice name in preferences." ); //NON-NLS
		}


		// check if totally equal locale
		for (Voice vox:flitevoices) {

			Locale loc = vox.getLocale();

			if (loc.getLanguage().equals(lang)&& loc.getCountry().equals(country) &&
				loc.getVariant().equals(variant)){
				Log.v("flite", "vox name corresponding to locale found : " + vox.getName()); //NON-NLS
				return vox.getName();
			}

		}

		//  check if language and country in common
		for (Voice vox:flitevoices) {

			Locale loc = vox.getLocale();

			if (loc.getLanguage().equals(lang)&& loc.getCountry().equals(country)){
				Log.v("flite", "vox name corresponding to locale found except for variant : "+ vox.getName()); //NON-NLS
				return vox.getName();
			}

		}

		// check if language is in common
		for (Voice vox:flitevoices) {

			Locale loc = vox.getLocale();

			if (loc.getLanguage().equals(lang)){
				Log.v("flite", "vox name corresponding to language found :" + vox.getName()); //NON-NLS
				return vox.getName();
			}

		}

		return null;

	}


	@Override
	public int onIsValidVoiceName(String voiceName){
		Log.v(LOG_TAG, "onIsValidVoiceName");
		return onLoadVoice(voiceName); // do I need to do it ?
	}

	@Override
	protected java.util.Set<String> onGetFeaturesForLanguage (String lang,
													String country,
													String variant){
		Log.v(LOG_TAG, "onFeatures");
		Set<String> myEmptySet = Collections.<String>emptySet();
		return myEmptySet;
	}

	// api21

	@Override
	public List<android.speech.tts.Voice> onGetVoices(){
		Log.v(LOG_TAG, "onGetVoices");
		List <android.speech.tts.Voice> voices = new ArrayList<android.speech.tts.Voice>();


		ArrayList<Voice>  flitevoices = CheckVoiceData.getAvailableVoices() ;
		Set<String> myEmptySet = Collections.<String>emptySet();
		for (Voice vox:flitevoices) {
			android.speech.tts.Voice v = new android.speech.tts.Voice(vox.getName(),  vox.getLocale(),
					android.speech.tts.Voice.QUALITY_NORMAL, android.speech.tts.Voice.LATENCY_NORMAL,
					false, myEmptySet);
			voices.add(v);
		}
		return voices;
	}


	@Override
	public int onLoadVoice(String voiceName){
		Log.v(LOG_TAG, "onLoadVoice");
		String[] voiceParams = voiceName.split("-");
		String language = voiceParams[0];
		String country = voiceParams[1];
		String variant = voiceParams[2];

		SharedPreferences settings = mContext.getSharedPreferences("test",0); //NON-NLS
		int rate = settings.getInt(voiceName , -1);
		if (rate > -1){
			defaultSpeechRate = rate;
		}

		return mEngine.isLanguageAvailable(language, country, variant);
	}


	@Override
	protected String[] onGetLanguage() {
		Log.v(LOG_TAG, "onGetLanguage");
		return new String[] {
				mLanguage, mCountry, mVariant
		};
	}

	@Override
	protected int onIsLanguageAvailable(String language, String country, String variant) {
		Log.v(LOG_TAG, "onIsLanguageAvailable" + language + " " + country + " " +variant); //NON-NLS
		return mEngine.isLanguageAvailable(language, country, variant);
	}

	@Override
	protected int onLoadLanguage(String language, String country, String variant) {
		Log.v(LOG_TAG, "onLoadLanguage"); //NON-NLS
		return mEngine.isLanguageAvailable(language, country, variant);
	}

	@Override
	protected void onStop() {
		Log.v(LOG_TAG, "onStop"); //NON-NLS
		mEngine.stop();
	}

	@Override
	protected synchronized void onSynthesizeText(
		SynthesisRequest request, SynthesisCallback callback) {
		Log.v(LOG_TAG, "onSynthesize"); //NON-NLS

		String language = request.getLanguage();
		String country = request.getCountry();
		String variant = request.getVariant();
		String text = request.getText();





		Integer speechrate = request.getSpeechRate();

		int result = 0; //true;

		result = mEngine.setLanguage(language, country, variant);
		/*
		if (! ((mLanguage.equals(language)) &&
				(mCountry.equals(country)) &&
				(mVariant.equals(variant) ))) {
			Log.v(LOG_TAG, "Setting to new language" + language + " " + country + " " + variant);
			result = mEngine.setLanguage(language, country, variant);
			mLanguage = language;
			mCountry = country;
			mVariant = variant;
			mEngine.setSpeechRate(speechrate);
			mSpeechRate = speechrate;
		}*/

		if (result != 0) {
			Log.e(LOG_TAG, "Could not set language for synthesis"); //NON-NLS
			return;
		}

		if (speechrate.intValue() != mSpeechRate.intValue())
		{
			mSpeechRate = speechrate;
			Log.v("flite_speed",  "setting rate"); //NON-NLS
			mEngine.setSpeechRate(speechrate);// /100 * defaultSpeechRate);
		}
		
		mCallback = callback;
		mCallback.start(mEngine.getSampleRate(), AudioFormat.ENCODING_PCM_16BIT, 1);
		mEngine.synthesize(text);
	}


	private final NativeFliteTTS.SynthReadyCallback mSynthCallback = new SynthReadyCallback() {
        @Override
        public void onSynthDataReady(byte[] audioData) {
            if ((audioData == null) || (audioData.length == 0)) {
                onSynthDataComplete();
                return;
            }

            final int maxBytesToCopy = mCallback.getMaxBufferSize();

            int offset = 0;

            while (offset < audioData.length) {
                final int bytesToWrite = Math.min(maxBytesToCopy, (audioData.length - offset));
                mCallback.audioAvailable(audioData, offset, bytesToWrite);
                offset += bytesToWrite;
            }
        }

        @Override
        public void onSynthDataComplete() {
            mCallback.done();
        }
	};

	/**
	 * Listens for language update broadcasts and initializes the flite engine.
	 */
	private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			initializeFliteEngine();
		}
	};
}
