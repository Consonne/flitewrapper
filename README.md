# Flite Wrapper for Android



This is based on :

https://github.com/happyalu/Flite-TTS-Engine-for-Android





## Main Differences

- It has been updated to Gradle
- A few bugs have been corrected
- Some layout improvements
- Uses EasyPersmissions so it can be compatible with android > 23
- It doesn't support audio streaming anymore ( temporary)
- You must use a slightly modified version of flite because it supports hts.
- It works only on android > kit kat because of appcompat





## Installing the application

You can download it on the Google Play Store:

https://play.google.com/store/apps/details?id=net.shallowmallow.speech.tts.flite



## Building this app from source



### Export the necessary environment variables

```bash
 
 export ANDROID_NDK=/path/to/android/ndk
 export ANDROID_SDK=/path/to/android/sdk
 export FLITEDIR=/path/to/flite-2.0.0
 
```



### Build the engine

```bash
cd "${FLITEDIR}"
for arch in armeabi armeabiv7a x86-64 mips aarch64;
do
	./configure --with-langvox=android2 --target="${arch}-android"
	make clean
	make -j8
done
cd "${FLITEDIR}"
```

### Build the apks

```bash
./gradlew assemble
```



### How to publish a release version

- Create the file 'gradle.properties'

  ```ini
  ABI_FILTERS = armeabi-v7a;x86
  RELEASE_STORE_FILE = file.keystore
  RELEASE_STORE_PASSWORD = store_password
  RELEASE_KEY_ALIAS = key_alias
  RELEASE_KEY_PASSWORD = key_password
  ```

  





## TODO

- Enable audio streaming at least for clustergen voices which already work
- Do dynamic loading for hts voices
- Make it work on gingerbread and with ant, do a version without appcompat, permissions 







## Need Basic Help



I have a few files NativeFliteTTS.java

And there is only one difference

- private int mNativeData;

+	private long mNativeData;

Do I really need to have different files  ? Seems stupid.

